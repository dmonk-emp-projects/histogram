-- null_algo
--
-- Do-nothing top level algo for testing
--
-- Dave Newbold, July 2013

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.ipbus.all;
use work.emp_data_types.all;
use work.emp_project_decl.all;

use work.emp_device_decl.all;
use work.emp_ttc_decl.all;

entity emp_payload is
	port(
		clk: in std_logic; -- ipbus signals
		rst: in std_logic;
		ipb_in: in ipb_wbus;
		ipb_out: out ipb_rbus;
		clk_payload: in std_logic_vector(2 downto 0);
		rst_payload: in std_logic_vector(2 downto 0);
		clk_p: in std_logic; -- data clock
		rst_loc: in std_logic_vector(N_REGION - 1 downto 0);
		clken_loc: in std_logic_vector(N_REGION - 1 downto 0);
		ctrs: in ttc_stuff_array;
		bc0: out std_logic;
		d: in ldata(4 * N_REGION - 1 downto 0); -- data in
		q: out ldata(4 * N_REGION - 1 downto 0); -- data out
		gpio: out std_logic_vector(29 downto 0); -- IO to mezzanine connector
		gpio_en: out std_logic_vector(29 downto 0) -- IO to mezzanine connector (three-state enables)
	);
		
end emp_payload;

architecture rtl of emp_payload is
	
	type dr_t is array(PAYLOAD_LATENCY downto 0) of ldata(3 downto 0);

	constant input_width : integer := 6;
	constant bin_width : integer := 32;
	constant threshold : integer := 500;

	signal data_in : lword := LWORD_NULL;
	signal counter : integer := 0;
	signal histogram_reset : std_logic := '0';
	signal histogram_output : lword := LWORD_NULL;
	signal max_bin_value : std_logic_vector(bin_width - 1 downto 0);


begin

	ipb_out <= IPB_RBUS_NULL;

	HistogramInstance : entity work.Histogram
	generic map (
		input_width => input_width,
		bin_width => bin_width
	)
	port map (
		clk => clk_p,
		data_in => data_in.data(input_width - 1 downto 0),
		data_valid => data_in.valid,
		reset => histogram_reset,
		data_out => histogram_output,
		max_bin_value => max_bin_value
	);

	pMain: process(clk_p)
	begin
		if rising_edge(clk_p) then
			data_in <= d(0);
			q(0) <= data_in;

			if counter = threshold - 1 then
				counter <= 0;
				histogram_reset <= '1';
			else
				counter <= counter + 1;
				histogram_reset <= '0';
			end if;
		end if;
	end process;

	q(1).valid <= histogram_output.valid;
	q(1).strobe <= '1';
	q(1).start <= '1';
	q(1).data <= histogram_output.data;
	
	bc0 <= '0';
	
	gpio <= (others => '0');
	gpio_en <= (others => '0');

end rtl;

