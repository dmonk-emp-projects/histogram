import yaml
import sys
import os
import shutil
from contextlib import contextmanager
import subprocess
import shlex


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def pullSources(config):
    ci_job_token = os.getenv("CI_JOB_TOKEN")
    sources = config['sources']
    if type(sources) == list:
        for source in sources:
            if 'gitlab' in source['url'] and 'https' in source['url']:
                url = source['url'][:8]
                url += "gitlab-ci-token:{}@".format(ci_job_token)
                url += source['url'][8:]
            else:
                url = source['url']
            cmd = "git clone {} --depth 1".format(url)
            if 'version' in source:
                cmd += " -b {}".format(source['version'])
            subprocess.call(shlex.split(cmd))
    elif type(sources) == dict:
        source = sources
        if 'gitlab' in source['url'] and 'https' in source['url']:
            url = source['url'][:8]
            url += "gitlab-ci-token:{}@".format(ci_job_token)
            url += source['url'][8:]
        else:
            url = source['url']
        cmd = "git clone {} --depth 1".format(url)
        if 'version' in source:
            cmd += " -b {}".format(source['version'])
        subprocess.call(shlex.split(cmd))
    subprocess.call([
        "rsync", "-ar", "{}".format(os.getenv("CI_PROJECT_DIR")), "./",
        "--exclude", "ci"
    ])


def buildWorkArea(config):
    if os.path.exists("ci/builds/work"):
        shutil.rmtree("ci/builds/work/src", ignore_errors=True)
        shutil.rmtree("ci/builds/work/proj", ignore_errors=True)
        os.makedirs("ci/builds/work/src")
        os.makedirs("ci/builds/work/proj")
    else:
        with cd("ci/builds"):
            subprocess.call(["ipbb", "init", "work"])
    with cd("ci/builds/work/src"):
        pullSources(config)


def main():
    if len(sys.argv) > 1:
        config_path = sys.argv[1]
    else:
        config_path = "ci/config.yaml"
    with open(config_path) as stream:
        config = yaml.safe_load(stream)
    buildWorkArea(config)


if __name__ == '__main__':
    main()

