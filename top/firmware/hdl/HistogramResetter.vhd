library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.emp_data_types.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;


entity HistogramResetter is
    port (
        --- Input Ports ---
        clk_p           : in std_logic;
        trigger_window  : std_logic_vector(36 - 1 downto 0) := X"0ffffffff";
        --- Output Ports ---
        histogram_reset : out std_logic
    );
end HistogramResetter;

architecture Behavioral of HistogramResetter is
    signal histogram_reset_counter : unsigned(36 downto 0) := (others => '0');
begin
    pMain : process(clk_p)
    begin
        if rising_edge(clk_p) then
            -- Threshold trigger
            -- if to_integer(unsigned(max_value_array(0))) > trigger_threshold then
            -- 	histogram_reset <= '1';
            -- else
            -- 	histogram_reset <= '0';
            -- end if;

            -- Fixed window trigger
            if histogram_reset_counter > unsigned(trigger_window)  - 1 then
                histogram_reset_counter <= (others => '0');
                histogram_reset <= '1';
            else
                histogram_reset <= '0';
                histogram_reset_counter <= histogram_reset_counter + 1;
            end if;
        end if;
    end process pMain;
end Behavioral;
