library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.emp_data_types.all;
use work.ipbus.all;
use work.ipbus_reg_types.all;


entity IPBusHistogram is
    generic (
        input_width : integer;
        bin_width   : integer;
        data_offset : integer := 4
    );
    port (
        --- Input Ports ---
        clk_p           : in std_logic;
        data_in         : in lword := LWORD_NULL;
        histogram_reset : in std_logic;
        --- Output Ports ---
        max_bin_value   : out std_logic_vector(bin_width - 1 downto 0);
        --- IPBus Ports ---
        clk             : in std_logic;
        rst             : in std_logic;
        ipb_in          : in ipb_wbus;
        ipb_out         : out ipb_rbus
    );
end IPBusHistogram;

architecture Behavioral of IPBusHistogram is
    signal ram_input        : std_logic_vector(bin_width - 1 downto 0);
    signal ram_addr         : std_logic_vector(input_width - 1 downto 0);
    signal ram_we           : std_logic := '0';
    signal histogram_output : lword := LWORD_NULL;
begin
    --==============================--
    mem1 : entity work.ipbus_dpram
    --==============================--
    generic map(
        ADDR_WIDTH => input_width,
        DATA_WIDTH => bin_width
    )
    port map(
        clk     => clk,
        rst     => rst,
        ipb_in  => ipb_in,
        ipb_out => ipb_out,
        rclk    => clk_p,
        we      => ram_we,
        d       => ram_input,
        addr    => ram_addr
    );

    --==============================--
    HistogramInstance : entity work.Histogram
    --==============================--
    generic map (
        input_width => input_width,
        bin_width   => bin_width
    )
    port map (
        clk           => clk_p,
        data_in       => data_in.data(data_offset + input_width - 1 downto data_offset),
        data_valid    => data_in.valid,
        reset         => histogram_reset,
        data_out      => histogram_output,
        max_bin_value => max_bin_value
    );

    pMain : process(clk_p)
        begin
            if rising_edge(clk_p) then
                if histogram_output.valid = '1' then
                    ram_addr <= histogram_output.data(32 + input_width - 1 downto 32);
                    ram_input <= histogram_output.data(bin_width - 1 downto 0);
                    ram_we <= '1';
                else
                    ram_we <= '0';
                end if;
            end if;
        end process pMain;
end Behavioral;
