library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.emp_data_types.all;


entity Histogram is
    generic (
        input_width : integer;
        bin_width   : integer
    );
    port (
        --- Input Ports ---
        clk           : in std_logic;
        data_in       : in std_logic_vector(input_width - 1 downto 0) := (others => '0');
        data_valid    : in std_logic := '0';
        reset         : in std_logic;
        --- Output Ports ---
        data_out      : out lword := LWORD_NULL;
        max_bin_value : out std_logic_vector(bin_width - 1 downto 0)
    );
end Histogram;

architecture Behavioral of Histogram is

    -- Internal reset signals
    signal reset_done          : std_logic := '1';
    signal reset_counter       : unsigned(input_width - 1 downto 0) := (others => '0');
    signal reset_output_buffer : std_logic := '0';

    -- Internal cache signals
    signal read_value      : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    signal read_cache      : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    signal write_value     : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    signal io_write_buffer : std_logic_vector(bin_width -1 downto 0) := (others => '0');
    signal read_buffer     : std_logic_vector(bin_width -1 downto 0) := (others => '0');

    -- Internal collision mitigation signals
    signal collision    : std_logic := '0';
    signal io_collision : std_logic := '0';
    signal offset       : integer   := 1;

    -- VHDL RAM signals
    attribute ram_style        : string;
    type tRAM is array(2**input_width - 1 downto 0) of std_logic_vector(bin_width - 1 downto 0);
    signal ram                 : tRAM := (others => (others => '0'));
    attribute ram_style of ram : signal is "block";

    -- Shift registers
    constant cShiftRegisterDepth      : integer                                            := 4;
    type tAddressShiftRegister is array(cShiftRegisterDepth - 2 downto 0) of std_logic_vector(input_width - 1 downto 0);
    signal address_shift_register     : tAddressShiftRegister                              := (others => (others => '0'));
    signal address_shift_register_out : std_logic_vector(input_width - 1 downto 0)         := (others => '0');
    signal valid_shift_register       : std_logic_vector(cShiftRegisterDepth - 2 downto 0) := (others => '0');
    signal valid_shift_register_out   : std_logic                                          := '0';

    -- Status signal
    signal max_value : std_logic_vector(bin_width - 1 downto 0) := (others => '0');

begin
    -- Set output port to be the maximum value signal
    max_bin_value <= max_value;

    pMain : process(clk)
    begin
        if rising_edge(clk) then
            if (reset = '1' or reset_done = '0') then
                -- Clear counter and flag before starting reset. Start condition should be: reset = '1' and reset_done = '1'. reset signal should be strobed to initiate reset.
                if reset_done = '1' then
                    reset_counter <= (others => '0');
                    reset_done <= '0';
                    address_shift_register(0) <= (others => '0');
                    max_value <= (others => '0');
                else
                    if to_integer(reset_counter) = 0 then
                        data_out.start <= '1';
                    else
                        data_out.start <= '0';
                    end if;
                        data_out.data(bin_width - 1 downto 0) <= ram(to_integer(reset_counter));
                        data_out.data(32 + input_width - 1 downto 32) <= std_logic_vector(reset_counter);
                        data_out.valid <= '1';
                        ram(to_integer(reset_counter)) <= (others => '0');
                        reset_counter <= reset_counter + 1;
                end if;
                -- Condition for ending reset.
                if reset_counter = (2**input_width - 1) then
                    reset_done <= '1';
                    -- reset all caching signals
                    read_value             <= (others => '0');
                    read_cache             <= (others => '0');
                    write_value            <= (others => '0');
                    io_write_buffer        <= (others => '0');
                    read_buffer            <= (others => '0');
                    collision              <= '0';
                    io_collision           <= '0';
                    address_shift_register <= (others => (others => '0'));
                    valid_shift_register   <= (others => '0');
                    max_value              <= (others => '0');
                end if;

            -- If not resetting, the input data stream must be placed into the histogram
            else
                -- Shift register to buffer the address until it is used to write the update value
                address_shift_register <= address_shift_register(address_shift_register'high - 1 downto address_shift_register'low) & data_in;
                address_shift_register_out <= address_shift_register(address_shift_register'high);
                valid_shift_register <= valid_shift_register(valid_shift_register'high - 1 downto valid_shift_register'low) & data_valid;
                valid_shift_register_out <= valid_shift_register(valid_shift_register'high);


                -- As the RAM I/O takes two clock cycles, it is possible that the input address repeats during this period.
                -- Condition 1 is when the write address = read address.
                -- Condition 2 is when there are two consectutive identical addresses
                -- Condition 3 is when the read address collides with an address in the middle of the shift register

                if address_shift_register(0) = address_shift_register_out and valid_shift_register_out = '1' then -- Condition 1
                    io_collision <= '1';
                    io_write_buffer <= write_value;
                else
                    io_collision <= '0';
                end if;

                if address_shift_register(1) = address_shift_register_out and valid_shift_register_out = '1' then -- Condition 3
                    collision <= '1';
                    read_cache <= write_value; -- Value to be written is cached and used on the next clock cycle instead of the value read from the RAM.
                else
                    collision <= '0';
                end if;

                if address_shift_register(1) = address_shift_register(2) and valid_shift_register(2) = '1' then -- Condition 2
                    -- In this case, the value read from the RAM, will be one less than it should be as the value has not yet been updated, therefore
                    -- the offset should be increased by one to account for this.
                    offset <= 2;
                else
                    offset <= 1;
                end if;

                if collision = '1' then
                    write_value <= std_logic_vector(unsigned(read_cache) + offset);
                else
                    write_value <= std_logic_vector(unsigned(read_value) + offset);
                end if;

                if io_collision = '1' then
                    read_value <= io_write_buffer;
                else
                    read_value <= read_buffer;
                end if;

                data_out <= LWORD_NULL;

                -- RAM I/O
                read_buffer <= ram(to_integer(unsigned(address_shift_register(0))));
                if valid_shift_register_out = '1' then
                    ram(to_integer(unsigned(address_shift_register_out))) <= write_value;
                end if;

                if unsigned(write_value) > unsigned(max_value) then
                    max_value <= write_value;
                end if;
            end if;
        end if;
    end process pMain;
end Behavioral;
